FROM ubuntu:latest

RUN apt update && apt dist-upgrade -y && apt install -y jq curl git grep sed bash
