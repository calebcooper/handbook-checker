#! /bin/bash

declare -g TARGET_PATH="${1}"
declare -g PAT="${2}"
declare -g CSV="${3}"
declare -g PROJECT_NUM="20382515"
declare -g ISSUE_URL="https://gitlab.com/api/v4/projects/${PROJECT_NUM}/issues"
declare -a KNOWN_PRIVATE=( 'docs.google.com' 'gitlab.zoom.us' 'gitlab.zendesk.com' 'gitlab-federal-support.zendesk.com' 'gitlab.com/groups' 'gitlabdemo.com' 'drive.google.com' 'gitlab.my.salesforce.com' 'example.com' 'caniuse.com' 'forum.gitlab.com' 'gitlab.com/gitlab-org/customers-gitlab-com' 'gitlab.com/gitlab-org/security-products' 'gitlab.com/gitlab-examples' '207.154.197.115' 'customers.gitlab.com' 'cloud.digitalocean.com/dashboard' 'gitlab-review.app' 'stats.pingdom.com' 'www.figma.com' 'calendar.google.com' 'storage.googleapis.com' 'open.spotify.com' 'manicode.us' 'security/vulnerability_report' 'gitlab-com/gl-infra' 'branches/new' )

func_link_checker() {
  declare FILE="${1}"
  declare -i RETURN_CODE
  declare -a BAD_URLS=()
  declare EXCLUDES=$( IFS='|'; echo "${KNOWN_PRIVATE[*]}")
  for URL in $(grep -Po '(?<=\()https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)(?=\))' "${FILE}" | grep -v -E "${EXCLUDES}" | sed 's/\.$//'); do 
    RETURN_CODE=$(curl --write-out "%{http_code}\n" --silent --output /dev/null "${URL}")
    if [[ ${RETURN_CODE} -eq 404 ]]; then
      BAD_URLS+=( "${URL}" )
    fi
  done
  URL=''
  if [[ ${#BAD_URLS[*]} -gt 0 ]]; then
    for URL in ${BAD_URLS[*]}; do
      for URL_LINE in $(grep -no ${URL} ${FILE} | egrep -v "\-\/issues\/[0-9]+" | sed -e 's/@/%40/g' -e 's/&/%26/g' -e 's/#/%23/g'); do
        FILE_LINK="https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/${FILE#*/}"
        HANDBOOK="${FILE##*handbook}"
        set -x
        DEAD_LINK="${URL_LINE#*:}"
        DEAD_LINK="${DEAD_LINK%\%23*}"
        if [[ ${DEAD_LINK} =~ "about.gitlab.com" ]]; then
          if [[ ${DEAD_LINK} =~ "handbook" ]]; then
            if [[ ${DEAD_LINK##*/} ]]; then
              FILE_PATH="sites/handbook/source${DEAD_LINK#*about.gitlab.com}${DEAD_LINK##*/}.md"
            else
              FILE_PATH="sites/handbook/source${DEAD_LINK#*about.gitlab.com}index.html.md"
            fi
          else
            if [[ ${DEAD_LINK##*/} ]]; then
              FILE_PATH="sites/uncategorized/source${DEAD_LINK#*about.gitlab.com}${DEAD_LINK##*/}.md"
            else
              FILE_PATH="sites/uncategorized/source${DEAD_LINK#*about.gitlab.com}index.html.md"
            fi
          fi
          COMMIT=$(git --git-dir www-gitlab-com/.git rev-list HEAD -- ${FILE_PATH} | head -1)
          COMMIT_LINK="https://gitlab.com/gitlab-com/www-gitlab-com/-/commit/${COMMIT}"
          DESCRIPTION="File: ${FILE_LINK}  Line: ${URL_LINE%%:*}  Link: ${DEAD_LINK}  Handbook: https://about.gitlab.com/handbook${HANDBOOK%.*}  Handbook commit: ${COMMIT_LINK}"
        else
          DESCRIPTION="File: ${FILE_LINK}  Line: ${URL_LINE%%:*}  Link: ${DEAD_LINK}  Handbook: https://about.gitlab.com/handbook${HANDBOOK%.*}"
        fi
        set +x
        TITLE_NUM=$((RANDOM % 5))
        case ${TITLE_NUM} in
          0)
            TITLE="The file ${FILE##*/} has a broken link: ${DEAD_LINK} at line ${URL_LINE%%:*}.";;
          1)
            TITLE="${FILE##*/} has a bad link: ${DEAD_LINK} at line ${URL_LINE%%:*}.";;
          2)
            TITLE="Bad link, ${DEAD_LINK}, found in ${FILE##*/} on line ${URL_LINE%%:*}.";;
          3)
            TITLE="${DEAD_LINK} is a bad link on line ${URL_LINE%%:*} which was found in ${FILE##*/}.";;
          4)
            TITLE="Found in ${FILE##*/}, on line ${URL_LINE%%:*}, the bad link ${DEAD_LINK} was.";;
        esac
        if [[ "${CSV}" ]]; then
          printf "%s,%s,%i,%s\n" "${FILE_LINK}" "${DEAD_LINK}" "${URL_LINE%%:*}" "https://about.gitlab.com/handbook${HANDBOOK%.*}" >> "${CSV}"
        elif [[ ! $(curl --header "PRIVATE-TOKEN: ${PAT}" "${ISSUE_URL}?search=${DESCRIPTION}" 2> /dev/null) =~ "${DESCRIPTION}" ]]; then
          printf "%s %s" "${TITLE}" "${DESCRIPTION}"
          curl -s --request POST --header "PRIVATE-TOKEN: ${PAT}" "${ISSUE_URL}?title=${TITLE// /%20}&description=${DESCRIPTION// /%0A}"
          printf "\n"
        fi
      done
    done
    while [[ -e lock.file ]]; do
      read -t 0.1 -s _
    done
    true > lock.file
    TOTAL_BAD_URLS=$((${#BAD_URLS[*]} + $(<bad_urls_count.txt)))
    printf "%i" "${TOTAL_BAD_URLS}" > bad_urls_count.txt
    rm -f lock.file
  fi
}

func_parallel() {
  declare -i PID=$$

  declare INPUT
  declare -i NUM_PROCESSES=20
  IFS=$'\n'
  mapfile -t INPUTS_ARRAY < <(find "${TARGET_PATH}" -name "*.md")
  for INPUT in ${INPUTS_ARRAY[*]}; do 
    mapfile -t CHILDREN < <(pgrep -P $PID)
    while [[ ${#CHILDREN[@]} -gt ${NUM_PROCESSES} ]]; do
      read -r -s -t 5
      mapfile -t CHILDREN < <(pgrep -P $PID)
    done
    func_link_checker "${INPUT}" &
  done
  wait
}

func_issue_cleaner() {
  set -x
  PAGE=1
  declare -a OLD_ISSUES=( $(curl -s -X GET --header "PRIVATE-TOKEN: ${PAT}" "${ISSUE_URL}?labels=None&created_before=$(date --date='-1 day' +%FT%TZ)&per_page=100" |  jq '.[].iid') )
  while [[ ${#OLD_ISSUES[*]} -gt 0 ]]; do
    declare -a OLD_ISSUES=( $(curl -s -X GET --header "PRIVATE-TOKEN: ${PAT}" "${ISSUE_URL}?labels=None&created_before=$(date --date='-1 day' +%FT%TZ)&per_page=100&page=${PAGE}" |  jq '.[].iid') )
    for OLD_ISSUE in ${OLD_ISSUES[*]}; do
      curl -s -X DELETE --header "PRIVATE-TOKEN: ${PAT}" "${ISSUE_URL}/${OLD_ISSUE}"
    done
    PAGE=$(( PAGE + 1 ))
  done
  set +x
}

func_issue_cleaner
printf "0" > bad_urls_count.txt
func_parallel
echo "$(<bad_urls_count.txt) bad urls found."
